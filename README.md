# go-helpers/server
- go pluggable http server framework
-  uses 
    - gorilla/mux
    - go-kit

## install
```
go get go get gitlab.com/go-helpers/server
```
or 
```
GO111MODULE=on go get gitlab.com/go-helpers/server@latest
```

## usage

```
package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/go-helpers/server"
)

func main() {
	svc := &service{}
	server := server.New().WithMiddleware().AttachService(svc)
	go server.Up()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	select {
	case <-quit:
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		server.Down(ctx)
	}
}

type service struct{}

func (s *service) Route(r *mux.Router) *mux.Router {
	r.Methods("GET", "POST").Path("/mamamia").HandlerFunc(exampleHandler)
	return r
}

func exampleHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "This is sn example endpoint")
}

```