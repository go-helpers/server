module gitlab.com/go-helpers/server

go 1.16

require (
	github.com/go-kit/kit v0.11.0 // indirect
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.4
)
