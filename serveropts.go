package server

import (
	"time"

	"github.com/gorilla/mux"
)

type serverOpts struct {
	readTimeout  *time.Duration // endpoint timeout
	writeTimeout *time.Duration // endpoint timeout
	router       *mux.Router
	port         int
	subRouters   map[string]*mux.Router
}

func defaultOpts() *serverOpts {
	return &serverOpts{
		router:     mux.NewRouter(),
		port:       8000,
		subRouters: make(map[string]*mux.Router),
	}
}

func New(port ...int) *serverOpts {
	return defaultOpts().setPort()
}

func (s *serverOpts) setPort(port ...int) *serverOpts {
	if len(port) != 0 {
		s.port = port[0]
	}
	return s
}

func (s *serverOpts) WithTimeout(timeouts ...time.Duration) *serverOpts {
	for i, timeout := range timeouts {
		switch i {
		case 0: //read timeout
			s.readTimeout = &timeout
		case 1: //write timeout
			s.writeTimeout = &timeout
		}
	}

	return s
}
func (s *serverOpts) WithMiddleware(middlewarefunc ...mux.MiddlewareFunc) *serverOpts {
	s.router.Use(middlewarefunc...)
	return s
}

// AttachHealthcheckService attaches a healthcheck endpoint to the root router
func (s *serverOpts) AttachHealthcheckService(service Service) serverIface {
	return s.AttachService(service)
}

// attach service to root router
func (s *serverOpts) AttachService(services ...Service) serverIface {
	for _, service := range services {
		service.Route(s.router)
	}
	return &server{
		s,
		nil,
	}
}
