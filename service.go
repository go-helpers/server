package server

import (
	"github.com/gorilla/mux"
)

type Service interface {
	Route(*mux.Router, ...string) *mux.Router
}
