package server

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

type serverIface interface {
	SubrouterService(*SubrouterOpts, ...Service) serverIface
	Up(...bool) error
	Down(ctx context.Context) error
}

type server struct {
	*serverOpts
	*http.Server
}

var _ serverIface = (*server)(nil)

//starts server and logs endpoints verbally if verbose=true
func (s *server) Up(verbose ...bool) error {
	var logEndpoints = false
	if len(verbose) > 0 {
		logEndpoints = verbose[0]
	}
	s.Server = &http.Server{
		Handler: s.router,
		Addr:    fmt.Sprintf(":%v", s.port),
	}
	if readTimeout := s.readTimeout; readTimeout != nil {
		s.Server.ReadTimeout = *readTimeout
	}
	if writeTimeout := s.writeTimeout; writeTimeout != nil {
		s.Server.WriteTimeout = *writeTimeout
	}

	if logEndpoints {
		s.serverOpts.router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
			tpl, _ := route.GetPathTemplate()
			met, _ := route.GetMethods()
			fmt.Println("server start with the following endpoints:")
			for _, m := range met {
				key := fmt.Sprintf("[%s] %s", m, tpl)
				fmt.Println(key)
			}
			fmt.Println("=====================================")
			return nil
		})
	}

	return s.Server.ListenAndServe()
}

// Down shuts down the server
func (s *server) Down(ctx context.Context) error {
	return s.Server.Close()

}

// AttachService attaches service to main router
func (s *server) AttachService(services ...Service) serverIface {
	for _, service := range services {
		service.Route(s.router)
	}
	return s
}

// SubrouterOpts options for subrouter
type SubrouterOpts struct {
	Name        string //Name must start with backslash, eg "/v1"
	Middlewares []mux.MiddlewareFunc
}

func (s *server) subrouter(opt *SubrouterOpts) *mux.Router {
	subrouter, ok := s.subRouters[opt.Name]
	if !ok {
		s.subRouters[opt.Name] = s.router.PathPrefix(opt.Name).Subrouter()
		subrouter = s.subRouters[opt.Name]
		subrouter.Use(opt.Middlewares...)
	}
	return subrouter
}

// SubrouterService attaches service to an existing subrouter, or creates a new one if non-existent
func (s *server) SubrouterService(opt *SubrouterOpts, services ...Service) serverIface {
	if len(services) != 0 {
		subrouter := s.subrouter(opt)
		for _, service := range services {
			service.Route(subrouter, opt.Name)
		}
	}
	return s
}
