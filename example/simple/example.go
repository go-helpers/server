package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gofrs/uuid"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/go-helpers/server"
)

func main() {
	svc := &service{}
	v1SubrouterOpts := &server.SubrouterOpts{
		Name: "api/{version}",
	}

	server := server.New().WithMiddleware(AddContext).
		AttachHealthcheckService(&healthcheck{}).
		SubrouterService(v1SubrouterOpts, svc)
	go server.Up()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	select {
	case <-quit:
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		server.Down(ctx)
	}
}

func AddContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// startTime/ := time.Now()
		ctx := context.WithValue(r.Context(), "context-id", uuid.Must(uuid.NewV4()).String())
		// logRespWriter := NewLogResponseWriter(w)
		next.ServeHTTP(w, r.WithContext(ctx))

		// fmt.Printf(
		// 	"duration=%s status=%d body=%s",
		// 	time.Since(startTime).String(),
		// 	logRespWriter.statusCode,
		// 	logRespWriter.buf.String())

	})
}

func loggingMiddleware(next http.Handler) http.Handler {
	return handlers.LoggingHandler(os.Stdout, next)
}

type service struct{}

func (s *service) Route(r *mux.Router, routerAlias ...string) *mux.Router {
	if len(routerAlias) != 0 {
		if routerAlias[0] == "/api/{version}" {
			r.Methods("GET", "POST").Path("/mamamia").HandlerFunc(exampleHandler)
		}
	}

	return r
}

func exampleHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	if ctxID := r.Context().Value("context-id"); ctxID != nil {
		fmt.Println("ctx-id", ctxID.(string))
	}
	fmt.Println("doer", r.RequestURI, vars["version"])
	time.Sleep(time.Second * 1)

	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Category: %v\n", vars["category"])
}

type healthcheck struct{}

func (s *healthcheck) Route(r *mux.Router, routerAlias ...string) *mux.Router {
	r.Methods("GET").Path("/health").HandlerFunc(healthcheckHasndler)
	return r
}

func healthcheckHasndler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "OK")
}
