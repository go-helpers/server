package main

import (
	"context"
	"net/http"
	"os"

	"github.com/go-kit/kit/log"
	"github.com/gofrs/uuid"
	"gitlab.com/go-helpers/server"
	"gitlab.com/go-helpers/server/example/extended/service"
)

func main() {

	logger := log.NewLogfmtLogger(os.Stderr)
	svc := service.New(nil, logger)
	server := server.New().WithMiddleware(AddContext).AttachService(svc)
	server.Up(true)

}

func AddContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// startTime/ := time.Now()
		ctxID := uuid.Must(uuid.NewV4()).String()
		ctx := context.WithValue(r.Context(), "context-id", ctxID)
		// logRespWriter := NewLogResponseWriter(w)
		w.Header().Set("XX-Request-ID", ctxID)
		next.ServeHTTP(w, r.WithContext(ctx))

		// fmt.Printf(
		// 	"duration=%s status=%d body=%s",
		// 	time.Since(startTime).String(),
		// 	logRespWriter.statusCode,
		// 	logRespWriter.buf.String())

	})
}
