package service

import (
	"github.com/go-kit/kit/log"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"gitlab.com/go-helpers/server"
)

type service struct {
	db         *sqlx.DB
	log        log.Logger
	serverOpts []httptransport.ServerOption
}

type example interface {
	server.Service
}

func New(db *sqlx.DB, logger log.Logger) example {
	return &service{
		db: db,
		serverOpts: []httptransport.ServerOption{
			httptransport.ServerErrorLogger(logger),
			httptransport.ServerErrorEncoder(EncodeErrorResponse),
			httptransport.ServerBefore(),
			httptransport.ServerAfter(),
			// httptransport.ServerBefore(ctxset.PopulateHeaders),
		},
	}
}

func (s *service) Route(r *mux.Router, subrouterName ...string) *mux.Router {
	s.newRouter(r)
	return r
}
