package service

import (
	"context"
	"net/http"

	"github.com/gorilla/mux"
)

func (svc *service) newRouter(mainRouter *mux.Router) *mux.Router {
	mainRouter.Methods("POST").Path("/user").Handler(svc.PostHandler())
	return mainRouter
}

func statusCodeHook(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(201)

	return nil
}
