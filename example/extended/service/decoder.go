package service

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type input struct {
	Name string `json:"name"`
}

// exampleDecoder decodes http request
func exampleDecoder(ctx context.Context, req *http.Request) (interface{}, error) {
	fmt.Println("requestID", ctx.Value("context-id"))
	var in input
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(b, &in); err != nil {
		return nil, err
	}
	return in, nil
}
