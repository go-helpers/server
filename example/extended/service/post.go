package service

import (
	"context"
	"fmt"

	"github.com/go-kit/kit/endpoint"
	httptransport "github.com/go-kit/kit/transport/http"
)

func PostEndpoint(svc *service) endpoint.Endpoint {

	return func(ctx context.Context, req interface{}) (interface{}, error) {
		fmt.Println("requestID", ctx.Value("context-id"))
		in := req.(input)

		fmt.Println(in.Name)
		return in, nil
	}
}

func (svc *service) PostHandler() *httptransport.Server {
	return httptransport.NewServer(
		PostEndpoint(svc),                  //use the endpoint
		exampleDecoder,                     //converts the parameters received via the request body into the struct expected by the endpoint
		EncodeJSONResponse(statusCodeHook), //converts the struct returned by the endpoint to a json response
		svc.serverOpts...,
	)
}
